// import components
import { useContext, useEffect } from 'react';
// page navigation to redirect the user back to the homepage or login page
import { Navigate } from 'react-router-dom';
// this module would need to consume the state of the user provided by the app.js module.
import UserContext from '../UserContext';

// create a function that will describe the procedures upon logging out and unmounting the user from the app.
// the logout has no physical entity => browser page will not be able to render any output on the display.
export default function Logout() {

	// the important purpose of this module is to 'unmount' the user from the application
	const { setUser, unsetUser } = useContext(UserContext);

	// clear out the saved token in the localStorage of the browser.
	unsetUser();

	// create a side effect. this will update the state of the global user.
	// this is to make sure that changes about the user will automatically/promptly be recognized in a global state.
	useEffect(() => {
		setUser({
			id: null,
			isAdmin: null
		})
	}, [setUser])

	return(
		<Navigate to="/login" replace={true} />
	)
}