// declare a state for the course details. we need to use the correct hook.
import { useState, useEffect } from 'react';

//this will serve as page whenever the client would want to select a single course/item from the catalog
import Hero from './../components/Banner';

//Grid System, Card, Button
import {Row, Col, Card, Button, Container} from 'react-bootstrap';

//import sweetalert
import Swal from 'sweetalert2';

//routing component
import { Link, useParams } from 'react-router-dom'; 

const data = {
	title: 'Welcome to B156 Booking-App',
	content: 'Check out our school campus'
}

export default function CourseView(){

	// state of our course details
	const [courseInfo, setCourseInfo] = useState({
		name: null,
		description: null,
		price: null
	});

	console.log(useParams())

	const {id} = useParams()
	console.log(id);

	useEffect(() => {
		fetch(`https://protected-beyond-20929.herokuapp.com/courses/${id}`).then(res => res.json()).then(convertedData => {
			setCourseInfo({
				name: convertedData.name,
				description: convertedData.description,
				price: convertedData.price
			})
		});
	}, [id])

    const enroll = () => {
    	return(
    		Swal.fire({
    		   icon: "success",
    		   title: 'Enrolled Successfully!',
    		   text: 'Thank you for enrolling to this course'
    		})
    	);
    }; 

	return(
	  <>
		<Hero bannerData={data} />
		<Row>
		   <Col>
		      <Container>
			      <Card className="text-center">
			         <Card.Body>
			            {/*<!-- Insert Comment Here --> */}
			            {/* Course Name */}
			         	<Card.Title>
			         		<h2> {courseInfo.name} </h2>
			         	</Card.Title>
			         	{/*  Course Description */}
			         	<Card.Subtitle>
			         		<h6 className="my-4"> Description: </h6>
			         	</Card.Subtitle>
			         	<Card.Text>
			         		{courseInfo.description}
			         	</Card.Text>
			         	{/*  Course Price */}
			         	<Card.Subtitle>
			         		<h6 className="my-4"> Price: </h6>
			         	</Card.Subtitle>
			         	<Card.Text>
			         		₱{courseInfo.price}
			         	</Card.Text>
			         </Card.Body>

			         <Button variant="warning" className="btn-block" onClick={enroll}> 
			            Enroll
			         </Button>

			         <Link className="btn btn-success btn-block mb-5" to="/login">
			         	Login to Enroll
			         </Link>
			      </Card>
		      </Container>
		   </Col>
		</Row>
	  </>
	);
}; 
