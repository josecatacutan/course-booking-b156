//identify needed components
import { Card } from 'react-bootstrap';

import { Link } from 'react-router-dom'

export default function CourseCard({courseProp}) {
	return(
		<Card className="m-4 d-md-inline-flex d-sm-inline-flex d-lg-inline-flex courseCard">
			<Card.Body>
				<Card.Title>
					{courseProp.name}
				</Card.Title>
				<Card.Text>
					{courseProp.description}
				</Card.Text>
				<Card.Text>
					Price: {courseProp.price}
				</Card.Text>
				<Link to={`view/${courseProp._id}`} className="btn btn-primary">
					View Course
				</Link>
			</Card.Body>
		</Card>
	);
};
